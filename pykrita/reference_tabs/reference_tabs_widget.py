# SPDX-FileCopyrightText: 2022 Freya Lupen <penguinflyer2222@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

try:
    from PyQt6.QtCore import QFileInfo, QMimeDatabase, pyqtSignal
    from PyQt6.QtGui import QImageReader, QAction, QActionGroup
    from PyQt6.QtWidgets import QWidget, QVBoxLayout, QMenuBar, QTabWidget, \
                                QFileDialog
except:
    from PyQt5.QtCore import QFileInfo, QMimeDatabase, pyqtSignal
    from PyQt5.QtGui import QImageReader
    from PyQt5.QtWidgets import QWidget, QVBoxLayout, QMenuBar, QTabWidget, \
                                QAction, QActionGroup, QFileDialog

# This is just a weird workaround for standalone vs. docker import
try:
    from reference_view_widget import ReferenceViewWidget
    from plugin_settings import PluginSettings, checkSettingAction
except:
    from .reference_view_widget import ReferenceViewWidget
    from .plugin_settings import PluginSettings, checkSettingAction


PLUGIN_CONFIG = "plugin/ReferenceTabsDocker" # constant string for kritarc config group

# The main widget, containing the menu bar and tab bar.
class ReferenceTabsWidget(QWidget):

    currentFolderChanged = pyqtSignal('QString')
    currentFolder = ""

    def __init__(self):
        super().__init__()

        self.settings = PluginSettings(PLUGIN_CONFIG)

        self.setAcceptDrops(True)

        layout = QVBoxLayout()
        self.setLayout(layout)

        menubar = QMenuBar()

        # - File menu
        fileMenu = menubar.addMenu("File")
        fileMenu.addAction("Open", self.openImage)
        fileMenu.addAction("Load", self.loadImage)
        fileMenu.addAction("Add Tab", self.addTab)

        closeMenu = fileMenu.addMenu("Close Tabs...")
        closeMenu.addAction("Close Current Tab", self.closeTab)
        closeMenu.addAction("Close All Tabs", self.closeAllTabs)
        closeMenu.addAction("Close Tabs to the Left", self.closeTabsLeft)
        closeMenu.addAction("Close Tabs to the Right", self.closeTabsRight)

        # - Image Setting menu
        settingsMenu = menubar.addMenu("Image Settings")

        zoomSettingsMenu = settingsMenu.addMenu("Zoom Setting")
        self.fitSettingGroup = QActionGroup(self)
        fitPageAction = QAction("Fit Page", self.fitSettingGroup)
        fitPageAction.setCheckable(True)
        fitPageAction.setData(1)
        fitWidthAction = QAction("Fit Width", self.fitSettingGroup)
        fitWidthAction.setCheckable(True)
        fitWidthAction.setData(2)
        fitHeightAction = QAction("Fit Height", self.fitSettingGroup)
        fitHeightAction.setCheckable(True)
        fitHeightAction.setData(3)
        fitFullsizeAction = QAction("Zoom 100%", self.fitSettingGroup)
        fitFullsizeAction.setCheckable(True)
        fitFullsizeAction.setData(4)

        checkSettingAction(self.settings.load("zoomSettingDefault", 1), self.fitSettingGroup) # Fit Page
        self.fitSettingGroup.triggered.connect(self.changeFitSetting)
        zoomSettingsMenu.addActions(self.fitSettingGroup.actions())

        scaleSettingsMenu = settingsMenu.addMenu("Scaling Mode Setting")
        self.scaleSettingGroup = QActionGroup(self)
        scaleSmoothAction = QAction("Smooth Scaling", self.scaleSettingGroup)
        scaleSmoothAction.setCheckable(True)
        scaleSmoothAction.setData(1)
        scaleFastAction = QAction("Sharp Scaling", self.scaleSettingGroup)
        scaleFastAction.setCheckable(True)
        scaleFastAction.setData(2)

        checkSettingAction(self.settings.load("scaleSettingDefault", 1), self.scaleSettingGroup) # Smooth
        self.scaleSettingGroup.triggered.connect(self.changeScaleSetting)
        scaleSettingsMenu.addActions(self.scaleSettingGroup.actions())

        settingsMenu.addAction("Change Background Color...", self.changeBGColor)


        # - UI Setting menu
        settingsMenu2 = menubar.addMenu("UI Settings")

        self.toggleToolbarAction = settingsMenu2.addAction("Show Toolbar")
        self.toggleToolbarAction.setCheckable(True)
        self.toggleToolbarAction.triggered.connect(self.setToolbarVis)
        self.toggleToolbarAction.setChecked(self.settings.load("showToolbar", True))

        tabPosSettingsMenu = settingsMenu2.addMenu("Tabs Position")
        self.tabPosSettingGroup = QActionGroup(self)
        tabPosAutoTLAction = QAction("Auto Top or Left", self.tabPosSettingGroup)
        tabPosAutoTLAction.setCheckable(True)
        tabPosAutoTLAction.setData(0b100) # 4
        tabPosAutoBLAction = QAction("Auto Bottom or Left", self.tabPosSettingGroup)
        tabPosAutoBLAction.setCheckable(True)
        tabPosAutoBLAction.setData(0b101) # 5
        tabPosAutoTRAction = QAction("Auto Top or Right", self.tabPosSettingGroup)
        tabPosAutoTRAction.setCheckable(True)
        tabPosAutoTRAction.setData(0b110) # 6
        tabPosAutoBRAction = QAction("Auto Bottom or Right", self.tabPosSettingGroup)
        tabPosAutoBRAction.setCheckable(True)
        tabPosAutoBRAction.setData(0b111) # 7
        tabPosTopAction = QAction("Top", self.tabPosSettingGroup)
        tabPosTopAction.setCheckable(True)
        tabPosTopAction.setData(QTabWidget.TabPosition.North) # 0
        tabPosLeftAction = QAction("Left", self.tabPosSettingGroup)
        tabPosLeftAction.setCheckable(True)
        tabPosLeftAction.setData(QTabWidget.TabPosition.West) # 2
        tabPosBottomAction = QAction("Bottom", self.tabPosSettingGroup)
        tabPosBottomAction.setCheckable(True)
        tabPosBottomAction.setData(QTabWidget.TabPosition.South) # 1
        tabPosRightAction = QAction("Right", self.tabPosSettingGroup)
        tabPosRightAction.setCheckable(True)
        tabPosRightAction.setData(QTabWidget.TabPosition.East) # 3

        tabPosAutoTLAction.setChecked(True)
        self.tabsPosition = self.settings.load("tabsPosition", 4) # auto (top/left)
        checkSettingAction(self.tabsPosition, self.tabPosSettingGroup)
        self.tabPosSettingGroup.triggered.connect(self.changeTabsPosition)
        tabPosSettingsMenu.addActions(self.tabPosSettingGroup.actions())

        tabsAutoHideAction = settingsMenu2.addAction("Hide Tabs When Solo", self.toggleAutoHideTabs)
        tabsAutoHideAction.setCheckable(True)
        tabsAutoHideAction.setChecked(self.settings.load("tabsHideSolo", True))


        layout.setMenuBar(menubar)
        # Don't overwrite Krita's application menubar on macOS.
        menubar.setNativeMenuBar(False)

        # Tab bar
        self.tabWidget = QTabWidget(self)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeRequestedTab)
        self.tabWidget.setMovable(True)
        self.tabWidget.setDocumentMode(True)
        self.tabWidget.currentChanged.connect(self.checkCorrectFitSetting)
        self.tabWidget.currentChanged.connect(self.checkCorrectScaleSetting)
        self.tabWidget.setTabBarAutoHide(tabsAutoHideAction.isChecked())
        layout.addWidget(self.tabWidget)

        self.filter = generateFiletypeFilter()


    def resizeEvent(self, event):
        if self.tabsPosition & 0b100 : # auto
            self.autoTabsPosition(event.oldSize())

        QWidget.resizeEvent(self, event)


    # Menu functions
    def openImage(self, filePath=False):
        self.addTab()
        self.tabWidget.setCurrentIndex(self.tabWidget.count()-1)
        self.loadImage(filePath)

    def loadImage(self, filePath=False):
        # Make sure there's a tab open first
        if self.tabWidget.count() == 0:
            self.addTab()

        # Attempt to load supported formats,
        # then return nothing if it's not valid
        if not filePath:
            filePath, _filter = QFileDialog.getOpenFileName(None, "Open an image", filter=self.filter, directory=self.currentFolder)
            self.setCurrentFolder(QFileDialog().directoryUrl().toLocalFile())
            if not filePath:
                return
        reader = QImageReader(filePath)
        # Automatically use rotation metadata (typically found in photographs)
        reader.setAutoTransform(True)
        image = reader.read()
        if image.isNull():
            return
        
        tabIdx = self.tabWidget.currentIndex()
        tab = self.tabWidget.widget(tabIdx)
        tab.setImage(image)
        tab.setFit(True)
        # Label the tab with the filename (sans path),
        # and tooltip with full path
        self.tabWidget.setTabText(tabIdx, QFileInfo(filePath).fileName())
        self.tabWidget.setTabToolTip(tabIdx, filePath)

    def setCurrentFolder(self, folder):
        self.currentFolder = folder
        self.currentFolderChanged.emit(self.currentFolder)

    def addTab(self):
        tab = ReferenceViewWidget(self.tabWidget)
        self.tabWidget.addTab(tab, "Empty")
        for action in self.fitSettingGroup.actions():
            if action.isChecked():
                tab.setFitSetting(action.data())
        for action in self.scaleSettingGroup.actions():
            if action.isChecked():
                tab.setScaleSetting(action.data())
        tab.setToolbarVis(self.toggleToolbarAction.isChecked())

    def closeTab(self):
        idx = self.tabWidget.currentIndex()
        self.closeRequestedTab(idx)

    def closeRequestedTab(self, idx):
        tab = self.tabWidget.widget(idx)
        if tab:
            self.tabWidget.removeTab(idx)
            tab.close()

    def closeAllTabs(self):
       while True:
            idx = self.tabWidget.currentIndex()
            if idx == -1:
                return
            self.closeRequestedTab(idx)

    def closeTabsLeft(self):
       while True:
            idxLeft = self.tabWidget.currentIndex() - 1
            if not self.tabWidget.widget(idxLeft):
                return
            self.closeRequestedTab(idxLeft)

    def closeTabsRight(self):
       while True:
            idxRight = self.tabWidget.currentIndex() + 1
            if not self.tabWidget.widget(idxRight):
                return
            self.closeRequestedTab(idxRight)

    def changeBGColor(self):
        tab = self.tabWidget.currentWidget()
        if tab:
            tab.changeBGColor()

    def setToolbarVis(self, vis):
        self.settings.save("showToolbar", vis)
        for tabIdx in range(0, self.tabWidget.count()):
            tab = self.tabWidget.widget(tabIdx)
            if tab:
                tab.setToolbarVis(vis)

    def changeTabsPosition(self, action):
        self.tabsPosition = action.data()

        if self.tabsPosition & 0b100: # auto
            self.autoTabsPosition()
        else:
            self.tabWidget.setTabPosition(QTabWidget.TabPosition(self.tabsPosition))

    def autoTabsPosition(self, oldWidgetSize=None):
        isHorizontal = self.size().width() > self.size().height()
        wasHorizontal = oldWidgetSize.width() > oldWidgetSize.height() if oldWidgetSize else None
        shouldReposition = oldWidgetSize == None or isHorizontal != wasHorizontal
        if shouldReposition:
            # 0b100 for auto
            # 0b0 for top / 0b1 for bottom
            # 0b00 for left / 0b10 for right
            if isHorizontal: # use vertical tabs
                self.tabWidget.setTabPosition(QTabWidget.TabPosition.East if self.tabsPosition & 0b10 else QTabWidget.TabPosition.West) # E(3)/W(2)
            else: # vertical or square; use horizontal tabs
                self.tabWidget.setTabPosition(QTabWidget.TabPosition(self.tabsPosition & 0b01)) # S(1)/N(0)

    def toggleAutoHideTabs(self):
        self.tabWidget.setTabBarAutoHide(not self.tabWidget.tabBarAutoHide())


    # Drag and drop support
    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def dropEvent(self, event):
        filePaths = event.mimeData().urls()
        # for now, always open in new tab
        for path in filePaths:
            # toLocalFile removes "file:///" on Windows
            # and "file://" on other OSes
            self.openImage(path.toLocalFile())

    def changeFitSetting(self, action):
        tab = self.tabWidget.currentWidget()
        if tab:
            tab.setFitSetting(action.data())

    def checkCorrectFitSetting(self):
        tab = self.tabWidget.currentWidget()
        if not tab:
            return
        for action in self.fitSettingGroup.actions():
            if action.data() == tab.fitSetting:
                action.setChecked(True)

    def changeScaleSetting(self, action):
        tab = self.tabWidget.currentWidget()
        if tab:
            tab.setScaleSetting(action.data())

    def checkCorrectScaleSetting(self):
        tab = self.tabWidget.currentWidget()
        if not tab:
            return
        for action in self.scaleSettingGroup.actions():
            if action.data() == tab.scalingMode:
                action.setChecked(True)

# Generate the formats filter for the file dialog.
def generateFiletypeFilter():
    # Ask QImage what files it can load.
    # With Krita, this will return more formats than standard Qt.
    imgFormats = QImageReader.supportedImageFormats()

    formatList = []
    filterList = []
    db = QMimeDatabase()

    for formatBytes in imgFormats:
        # convert QByteArray to string and prepend "*."
        formatList.append(f"*.{str(formatBytes, 'utf-8')}")

    for format in formatList:
        formatDesc = db.mimeTypeForFile(format).comment()
        # Some formats (camera raw) don't have proper entries, so hide those.
        # They're still listed in "All supported formats".
        if formatDesc != "unknown":
            # "Krita document (*.kra)", etc
            filterList.append(f"{formatDesc} ({format})")

    formatAllString = " ".join(formatList)
    # Alphabetical by description,
    # then "All supported formats (*.bmp *.kra *.png ...)" first.
    filterList.sort()
    filterList.insert(0, f"All supported formats ({formatAllString})")

    return ";;".join(filterList)
