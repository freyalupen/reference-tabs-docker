# SPDX-FileCopyrightText: 2022 Freya Lupen <penguinflyer2222@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later


from krita import DockWidget, DockWidgetFactory, DockWidgetFactoryBase

from .reference_tabs_widget import ReferenceTabsWidget

DOCKER_ID = 'pykrita_reference_tabs'

# constant string for the config group in kritarc
PLUGIN_CONFIG = "plugin/ReferenceTabsDocker"


class ReferenceTabsDocker(DockWidget):

    def __init__(self):
        super().__init__()

        widget = ReferenceTabsWidget()

        # Load config from kritarc
        widget.currentFolder = Krita.readSetting(PLUGIN_CONFIG, "currentFolder", "")
        widget.currentFolderChanged.connect(writeCurrentFolderToConfig)

        self.setWindowTitle("Reference Tabs")
        self.setWidget(widget)

    # This override is required.
    def canvasChanged(self, canvas):
        pass

def writeCurrentFolderToConfig(currentFolder):
    Krita.writeSetting(PLUGIN_CONFIG, "currentFolder", currentFolder)


Krita.addDockWidgetFactory(DockWidgetFactory(
    DOCKER_ID, DockWidgetFactoryBase.DockPosition.DockLeft, ReferenceTabsDocker))
