# SPDX-FileCopyrightText: 2024 Freya Lupen <penguinflyer2222@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later


try:
    from krita import Krita
except:
    Krita = False
    try:
        from PyQt6.QtCore import QSettings
    except:
        from PyQt5.QtCore import QSettings
    import os.path

# Class to handle saving/loading settings to kritarc if running from Krita
# or a settings.ini in the plugin folder if not
class PluginSettings():

    def __init__(self, configString):
        self.PLUGIN_CONFIG = configString

        if not Krita:
            self.settings = QSettings(os.path.join(os.path.dirname(__file__), "settings.ini"), QSettings.Format.IniFormat)

    def load(self, settingName, defaultValue):
        if Krita:
            value = Krita.instance().readSetting(self.PLUGIN_CONFIG, settingName, str(defaultValue))
        else:
            value = self.settings.value(settingName, defaultValue)

        if defaultValue.__class__ is str:
            return value
        elif defaultValue.__class__ is int:
            return int(value)
        elif defaultValue.__class__ is bool:
            # Krita "True", QSettings "true"
            if value.__class__ is str:
                value = value.lower() == "true"
            return value
        elif defaultValue.__class__ is float:
            return float(value)
        else:
            # print(f"Warning: unhandled loadSetting type {defaultValue.__class__}, this may cause errors")
            return defaultValue

    def save(self, settingName, settingValue):
        if Krita:
            Krita.instance().writeSetting(self.PLUGIN_CONFIG, settingName, str(settingValue))
        else:
            self.settings.setValue(settingName, settingValue)

# convenience function
def checkSettingAction(settingValue, actionGroup):
    for action in actionGroup.actions():
        if action.data() == settingValue:
            action.setChecked(True)
