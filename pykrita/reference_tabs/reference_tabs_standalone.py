# SPDX-FileCopyrightText: 2022 Freya Lupen <penguinflyer2222@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import argparse
try:
    from PyQt6.QtCore import Qt, QLibraryInfo
    from PyQt6.QtWidgets import QApplication
except:
    from PyQt5.QtCore import Qt, QLibraryInfo
    from PyQt5.QtWidgets import QApplication

from reference_tabs_widget import ReferenceTabsWidget

# This is a standalone app version of the docker for testing purposes.
# It depends only on Python and PyQt5.
# It has the same functionality, except for the Krita-dependent things:
# * Krita's icons.
# * Krita's extended image format support.
# * The ability to save configuration.

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file_path", help="image file path(s)", type=str, nargs='*')
    parser.add_argument("--always_on_top", help="enables window always on top", action='store_true')
    args = parser.parse_args()
    paths = args.file_path
    alwaysOnTop = args.always_on_top

    if QLibraryInfo.version().majorVersion() == 5: # PyQt5
          QApplication.setAttribute(Qt.ApplicationAttribute.AA_EnableHighDpiScaling)
    QApplication.setHighDpiScaleFactorRoundingPolicy(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    app = QApplication([])

    app.setApplicationName("Reference Tabs")

    # Use Fusion style for expected look and behavior
    try:
        from PyQt6.QtWidgets import QStyleFactory
    except:
        from PyQt5.QtWidgets import QStyleFactory
    app.setStyle(QStyleFactory.create("Fusion"))

    widget = ReferenceTabsWidget()
    # Similar to my usual size for the docker.
    widget.resize(400, 700)
    widget.setWindowFlag(Qt.WindowType.WindowStaysOnTopHint, alwaysOnTop)
    widget.show()

    for path in paths:
        widget.openImage(path)

    sys.exit(app.exec())
