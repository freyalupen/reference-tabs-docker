### Reference Tabs Docker 

This is a Python plugin for Krita: a docker for containing reference images in separate tabs, with basic view manipulation (zoom, pan, rotate, mirror). For more details, see the Manual.html.

### Installing Krita Plugins
See the Krita manual page, [Managing Python plugins](https://docs.krita.org/en/user_manual/python_scripting/install_custom_python_plugin.html).

The short version:
- Import the .zip file with Tools->Scripts->Import Python Plugin
Or
- Extract the .zip file and put the contents of the `pykrita` folder inside `<your krita resource folder>/pykrita`.

Then
- Restart Krita. Enable the plugin in Configure Krita->Python Plugin Manager. Then restart Krita again.
